const express = require('express')
const cors = require('cors')
require('./db/mongoose')
const categoryRouter = require('./routers/category')
const productRouter = require('./routers/product')

const app = express()
const port = process.env.PORT

app.use(cors())
app.use(express.json())
app.use(categoryRouter)
app.use(productRouter)

app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})