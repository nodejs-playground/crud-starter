const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    imgPath: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        trim: true
    }
})

const Category = mongoose.model('Category', categorySchema)

module.exports = Category