const express = require('express')
const Category = require('../model/category')
const router = new express.Router()

router.post('/categories', async(req, res) => {
    try {
        const category = new Category(req.body)
        await category.save()
        res.status(201).send(category)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/categories', async(req, res) => {
    try {
        const categories = await Category.find();
        res.status(200).send(categories)
    } catch(e) {
        res.send(500).send()   
    }
})

router.get('/categories/:id', async(req, res) => {
    try {
        const category = await Category.findById(req.params.id)
        if(!category) {
            return res.status(404).send()
        }
        res.status(200).send(category)
    } catch(e) {
        res.status(500).send()
    }
})



module.exports = router
