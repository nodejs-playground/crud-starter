const express = require('express')
const Product = require('../model/product')
const router = new express.Router()

router.get('/products', async(req, res) => {
    try {
        const products = await Product.find()
        res.send(products)
    } catch (e) {
        res.status(400).send()
    }
})

router.get('/products/:id', async(req, res) => {
    try {
        const product = await Product.findById(req.params.id)
        if(!product)
            return res.status(404).send()

        res.send(product)
    } catch (e) {
        res.status(400).send()
    }
})

router.post('/products', async(req, res) => {
    try {
        console.log(req.body)
        const product = new Product(req.body)
        await product.save()
        res.status(201).send(product)
    } catch(e) {
        console.log('err creating ', e)
        res.status(400).send(e)
    }
})

router.patch('/products/:id', async(req, res) => {
    console.log('editing...', req.params.id)
    const updates = Object.keys(req.body)
    const availabelUpdates = ['title', 'description', 'image', 'sku', 'price', 'quantity']
    const isValid = updates.every(update => availabelUpdates.includes(update))

    if(!isValid)
        return res.status(400).send('Invalid field update')

    try{
        const product = await Product.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true})
        if(!product) 
            return res.status(404).send()

        res.send(product)
    }catch(e) {
        res.status(500).send()
    }
})

router.delete('/products/:id', async(req, res) => {
    console.log('deleting...', req.params.id)
    try {
        const product = await Product.findByIdAndDelete(req.params.id)
        if(!product)
            return res.status(404).send()

        res.send(product._id)
    } catch(e) {
        res.status(500).send()
    }
})

module.exports = router