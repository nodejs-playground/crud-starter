This project showcase a basic CRUD operations for product management using NodeJS, ExpressJS and connecting to MongoDB.

To get started, clone this project to your local directory.
Then run "npm install" to download packages used in this project like mongodb, mongoose, express, nodemon(only in dev environment).
Then run "npm dev run" command.

This project has been deployed to Heroku and database is deployed to MongoDB Atlas which are free for normal usage.

For deployement, firstly setup SSH keypairs to secure communication among machines. This setup and configuration only have to do once.    

Run below in Gitbash on Windows machine:
1) ssh-keygen -t rsa -b 4096 -C "email"
2) ls -a -l ~/.ssh (to list keypairs, should see id_rsa & id_rsa.pub keypairs now)
3) ssh-add ~/.ssh/id_rsa to add the identity to your machine.
4) cat ~/.ssh/id_rsa.pub to get the public key. Go to your repo account, click on settings > SSH and add the public key to the text field. 
5) ssh -T git@gitlab.com to test your connection which should show you a greeting message. 

Secondly push your project to your git repository. 
Thirdly push your project to Heroku.

Run below on Gitbash to deploy application to Heroku: 
1) heroku keys:add to setup SSH public key with Heroku.
2) heroku create "project-name" to create application.
3) heroku config:set "key"="value" to setup environment variables of our application. As this project connect to MongoDB, deploy your database to MongoDB Atlas to get the public URL so that your application deployed on Heroku can connect to it.
4) git push heroku master to push project to heroku.


